use crate::middleware::auth;
use actix_web::{App, HttpServer};

pub async fn start_server() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .wrap(auth::Authentication::default().add_ignored_routes(vec![
                "/_health".to_string(),
                "/auth/login".to_string(),
                "/auth/register".to_string(),
                "/auth/refresh".to_string(),
                "/auth/verify-email/{token}".to_string(),
                "/auth/resend-verification".to_string(),
            ]))
            .configure(crate::application::configure::configure)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
