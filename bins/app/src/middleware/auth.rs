use actix_web::{
    dev::{forward_ready, Service, ServiceRequest, ServiceResponse, Transform},
    Error,
};
use error;
use futures::future;
use std::future::{ready, Ready};
use support::helpers::auth::verify_access_token;

// There are two steps in middleware processing.
// 1. Middleware initialization, middleware factory gets called with
//    next service in chain as parameter.
// 2. Middleware's call method gets called with normal request.
#[derive(Default)]
pub struct Authentication {
    // add a field to store the ignored routes
    pub skip: Vec<String>,
}
impl Authentication {
    // add a method to add ignored routes
    pub fn add_ignored_routes(mut self, mut routes: Vec<String>) -> Self {
        self.skip.append(&mut routes);
        self
    }
}

// Middleware factory is `Transform` trait
// `S` - type of the next service
// `B` - type of response's body
impl<S, B> Transform<S, ServiceRequest> for Authentication
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type InitError = ();
    type Transform = AuthenticationMiddleware<S>;
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ready(Ok(AuthenticationMiddleware {
            service,
            skip: self.skip.clone(),
        }))
    }
}

pub struct AuthenticationMiddleware<S> {
    service: S,
    skip: Vec<String>,
}

impl<S, B> Service<ServiceRequest> for AuthenticationMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = Error>,
    S::Future: 'static,
    B: 'static,
{
    type Response = ServiceResponse<B>;
    type Error = Error;
    type Future =
        future::Either<future::Ready<Result<ServiceResponse<B>, actix_web::Error>>, S::Future>;

    forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let skip = self.skip.contains(&req.match_pattern().unwrap_or_default());
        if skip {
            return future::Either::Right(self.service.call(req));
        }

        let headers = req.headers();
        let access_token = headers.get("authorization");
        let access_token_str = match access_token {
            Some(value) => std::str::from_utf8(value.as_bytes())
                .unwrap_or_default()
                .split_whitespace()
                .nth(1)
                .unwrap_or_default(),
            None => "",
        };

        match verify_access_token(access_token_str) {
            //if the result is Ok, proceed with calling the service
            Ok(_) => {
                let fut = self.service.call(req);
                future::Either::Right(fut)
            }
            //if the result is Err, return the error as a ServiceResponse
            Err(_) => {
                 future::Either::Left(future::err(
                    error::Error::Unauthorized("user_session_expired".to_string()).into(),
                ))
            }
        }
    }
}
