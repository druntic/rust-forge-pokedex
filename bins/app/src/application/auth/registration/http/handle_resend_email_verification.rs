use crate::application::auth::registration::contract;
use crate::application::auth::registration::data;
use actix_web::*;
use config;
use support::helpers::http;
use support::helpers::mail;
use validr::Validation;

pub async fn handle_resend_email_verification<T: contract::RegistrationContract>(
    data: web::Json<data::ResendVerificationData>,
    service: web::Data<T>,
    request: HttpRequest,
) -> Result<HttpResponse, Error> {
    let email = data.into_inner().validate()?.convert();
    let action_token = service.resend_email_verification(email.clone()).await?;
    let url = http::get_base_url(&request);
    mail::send_verification_mail(&action_token.token, &email, url).await?;
    let is_dev = config::get_default("IS_DEV", "false");
    if is_dev == "true" {
        Ok(HttpResponse::Ok()
            .append_header(("X-Res-Token", action_token.token))
            .body("email verification sent"))
    } else {
        Ok(HttpResponse::Ok().body("email verification sent"))
    }
}
