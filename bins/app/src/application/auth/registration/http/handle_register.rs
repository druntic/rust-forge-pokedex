use crate::application::auth::registration::contract;
use crate::application::auth::registration::data;
use actix_web::*;
use config;
use support::helpers::http;
use support::helpers::mail;
use validr::Validation;

pub async fn handle_register<T: contract::RegistrationContract>(
    data: web::Json<data::RegistrationUserData>,
    _request: HttpRequest,
    service: web::Data<T>,
) -> Result<HttpResponse, Error> {
    let registration_data = data.into_inner().validate()?.convert();
    let email = registration_data.email.clone();
    let action_token = service.register(registration_data).await?;
    //send email
    let url = http::get_base_url(&_request);
    mail::send_verification_mail(&action_token.token, &email, url).await?;

    //return response with token in header if IS_DEV=true
    let is_dev = config::get_default("IS_DEV", "false");
    if is_dev == "true" {
        Ok(HttpResponse::Ok()
            .append_header(("X-Res-Token", action_token.token))
            .body("User registered and email verification sent"))
    } else {
        Ok(HttpResponse::Ok().body("User registered and email verification sent"))
    }
}
