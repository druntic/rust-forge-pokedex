use crate::application::auth::registration::contract::{
    PgRepositoryContract, PgServiceContract, RegistrationContract,
};
use crate::application::auth::registration::data::CreateNewUserData;
use async_trait::async_trait;
use error::Error;
use support::helpers::auth;
use support::store::models::action_token::ActionToken;

pub struct Registration<A: PgRepositoryContract, B: PgServiceContract> {
    pub repository: A,
    pub service: B,
}

#[async_trait]
impl<A, B> RegistrationContract for Registration<A, B>
where
    A: PgRepositoryContract + Sync + Send,
    B: PgServiceContract + Sync + Send,
{
    //function to register new user into the database
    async fn register(&self, mut data: CreateNewUserData) -> Result<ActionToken, Error> {
        //first check if user exists in database already
        let user_exists = self.repository.exists(data.email.as_str()).await;
        if user_exists.is_ok() {
            return Err(Error::BadRequest("invalid mail".to_string()));
        }
        //continue with creating user
        data.password = auth::hash_password(&data.password)?;
        let created_user = self.service.create_user(data).await?;
        //create action token in database
        self.service
            .create_action_token("registration_verification".to_string(), created_user.id)
            .await
    }
    //function to resend email verification for user
    async fn resend_email_verification(&self, email: String) -> Result<ActionToken, Error> {
        let user = self.repository.exists(email.as_str()).await?;
        if user.email_verified_at.is_some() {
            return Err(Error::BadRequest("something went wrong".to_string()));
        }
        //create action token in database
        self.service
            .create_action_token("registration_verification".to_string(), user.id.clone())
            .await
    }
}
