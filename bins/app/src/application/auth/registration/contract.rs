use crate::application::auth::registration::data::CreateNewUserData;
use async_trait::async_trait;
use error::Error;
use support::store::models::action_token::ActionToken;
use support::store::models::user::User;

#[async_trait]
pub trait RegistrationContract {
    // Define the function signature
    async fn register(&self, data: CreateNewUserData) -> Result<ActionToken, Error>;
    async fn resend_email_verification(&self, email: String) -> Result<ActionToken, Error>;
}

#[async_trait]
pub trait PgServiceContract {
    async fn create_user(&self, data: CreateNewUserData) -> Result<User, Error>;
    async fn create_action_token(
        &self,
        action_name: String,
        user_id: String,
    ) -> Result<ActionToken, Error>;
}

#[async_trait]
pub trait PgRepositoryContract {
    async fn exists(&self, email: &str) -> Result<User, Error>;
}
