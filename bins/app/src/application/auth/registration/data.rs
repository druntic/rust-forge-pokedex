use chrono;
use diesel::{prelude::Queryable, Insertable};
use serde::{Deserialize, Serialize};
use support::schema::{action_tokens, users};
use validr::*;

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]

pub struct RegistrationUserData {
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub email: Option<String>,
    pub password: Option<String>,
}

// define a new struct that corresponds to the database table
#[derive(Insertable, Debug, Clone, Serialize)]
#[diesel(table_name = users)]
#[diesel(treat_none_as_null = true)]
pub struct CreateNewUserData {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub password: String,
}

// implement a method to convert RegistrationUserData to NewUser
impl RegistrationUserData {
    // convert RegistrationUserData to NewUser
    pub fn convert(self) -> CreateNewUserData {
        // unwrap the optional fields and assign them to variables
        let first_name = self.first_name.unwrap();
        let last_name = self.last_name.unwrap();
        let email = self.email.unwrap();
        let password = self.password.unwrap();
        // create a new NewUser instance with the unwrapped values
        CreateNewUserData {
            first_name,
            last_name,
            email,
            password,
        }
    }
}

impl Validation for RegistrationUserData {
    fn modifiers(&self) -> Vec<Modifier<Self>> {
        vec![]
    }

    fn rules(&self) -> Vec<Rule<Self>> {
        vec![
            rule_required!(first_name),
            rule_required!(last_name),
            rule_required!(email),
            rule_required!(password),
            rule_email!(email),
            rule_length_min!(password, 6),
            Rule::new(
                "password_contains_number",
                |obj: &Self, error: &mut validr::error::ValidationError| {
                    if let Some(password) = &obj.password {
                        if !password.chars().any(|c| c.is_digit(10)) {
                            error.add("password_missing_number");
                        }
                    }
                },
            ),
        ]
    }
}

#[derive(Insertable, Debug, Clone, Serialize, Queryable)]
#[diesel(table_name = action_tokens)]
#[diesel(treat_none_as_null = true)]
pub struct CreateNewActionTokenData {
    pub entity_id: String,
    pub token: String,
    pub action_name: String,
    pub expires_at: chrono::NaiveDateTime,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct ResendVerificationData {
    pub email: Option<String>,
}
impl Validation for ResendVerificationData {
    fn modifiers(&self) -> Vec<Modifier<Self>> {
        vec![]
    }

    fn rules(&self) -> Vec<Rule<Self>> {
        vec![rule_required!(email), rule_email!(email)]
    }
}

impl ResendVerificationData {
    pub fn convert(self) -> String {
        // unwrap the optional fields and assign them to variables
        self.email.unwrap()
    }
}
