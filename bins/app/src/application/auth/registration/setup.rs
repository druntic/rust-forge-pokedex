use crate::application::auth::registration::domain;
use crate::application::auth::registration::domain::Registration;
use crate::application::auth::registration::http::{
    handle_register, handle_resend_email_verification,
};
use crate::application::auth::registration::infrastructure::{PgRepository, PgService};
use actix_web::web;

pub fn routes(cfg: &mut web::ServiceConfig) {
    let service = domain::Registration {
        service: PgService {},
        repository: PgRepository {},
    };
    cfg.app_data(web::Data::new(service));
    cfg.route(
        "/auth/register",
        web::post().to(handle_register::handle_register::<Registration<PgRepository, PgService>>),
    );

    cfg.route(
        "/auth/resend-verification",
        web::post().to(
            handle_resend_email_verification::handle_resend_email_verification::<
                Registration<PgRepository, PgService>,
            >,
        ), // use a get method and call the verify_email function
    );
}
