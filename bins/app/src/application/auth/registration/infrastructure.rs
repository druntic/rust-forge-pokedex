use super::contract::{PgRepositoryContract, PgServiceContract};
use crate::application::auth::registration::data::{CreateNewActionTokenData, CreateNewUserData};
use async_trait::async_trait;
use chrono::{Duration, Utc};
use diesel::prelude::*;
use error::Error;
use std::env;
use support::helpers::action_token::generate_token;
use support::schema::{action_tokens, users};
use support::store::models::action_token::ActionToken;
use support::store::models::user::User;

pub struct PgService {}

pub struct PgRepository {}

#[async_trait]
impl PgServiceContract for PgService {
    async fn create_user(&self, data: CreateNewUserData) -> Result<User, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        diesel::insert_into(users::table)
            .values(data)
            .get_result(&mut connection)
            .map_err(Error::from)
    }

    async fn create_action_token(
        &self,
        action_name: String,
        user_id: String,
    ) -> Result<ActionToken, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        let data = CreateNewActionTokenData {
            entity_id: user_id,
            token: generate_token(16),
            action_name,
            expires_at: Utc::now().naive_utc()
                + Duration::seconds(
                    env::var("EMAIL_VERIFICATION_EXPIERY_TIME_SECONDS")
                        .expect("expeiery time must be set")
                        .parse::<i64>()
                        .expect("EMAIL_VERIFICATION_EXPIERY_TIME_SECONDS is not a valid number"),
                ),
        };

        diesel::insert_into(action_tokens::table)
            .values(&data)
            .get_result(&mut connection)
            .map_err(Error::from)
    }
}
#[async_trait]
impl PgRepositoryContract for PgRepository {
    async fn exists(&self, email: &str) -> Result<User, Error> {
        let users = User::exists(email).await?;
        if !users.is_empty() {
            return Ok(users[0].clone());
        } else {
            return Err(Error::BadRequest("something went wrong".to_string()));
        }
    }
}
