use crate::application::auth::registration::data::RegistrationUserData;
use actix_web::web;
use assert_matches::assert_matches;
use error::Error;
use validr::Validation;

#[actix_web::test]
async fn test_registration_validation_last_name() {
    let user_data = RegistrationUserData {
        first_name: Some("Test".to_string()),
        last_name: Some("".to_string()),
        email: Some("test@example.com".to_string()),
        password: Some("password".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}

#[actix_web::test]
async fn test_registration_validation_first_name() {
    let user_data = RegistrationUserData {
        first_name: Some("".to_string()),
        last_name: Some("sdfds".to_string()),
        email: Some("test@example.com".to_string()),
        password: Some("password".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}

#[actix_web::test]
async fn test_registration_validation_email() {
    let user_data = RegistrationUserData {
        first_name: Some("sdfsd".to_string()),
        last_name: Some("sdfds".to_string()),
        email: Some("".to_string()),
        password: Some("password".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}

#[actix_web::test]
async fn test_registration_validation_password() {
    let user_data = RegistrationUserData {
        first_name: Some("sdfsd".to_string()),
        last_name: Some("sdfds".to_string()),
        email: Some("test@example.com".to_string()),
        password: Some("".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}
