use actix_web::cookie::Cookie;
use async_trait::async_trait;
use error::Error;
use support::store::models::action_token::ActionToken;
use support::store::models::user::User;

#[async_trait]
pub trait AuthenticationContract {
    async fn verify_email(&self, entity_id: String) -> Result<(String, String), Error>;
    async fn login(&self, email: String, password: String) -> Result<(String, String), Error>;
    async fn logout(&self, cookie: Option<Cookie<'_>>) -> Result<usize, Error>;
    async fn refresh(&self, cookie: Option<Cookie<'_>>) -> Result<String, Error>;
}

#[async_trait]
pub trait PgServiceContract {
    async fn update_token_action_name(
        &self,
        token_id: &str,
        action_name: &str,
    ) -> Result<usize, Error>;

    async fn update_executed_at(&self, id: &str) -> Result<usize, Error>;
    async fn update_email_verified_at(&self, id: &str) -> Result<usize, Error>;
    async fn update_refresh_token(&self, id: &str, refresh_token: &str) -> Result<usize, Error>;
}

#[async_trait]
pub trait PgRepositoryContract {
    async fn exists(&self, email: &str) -> Result<User, Error>;
    async fn get_action_token_by_token(&self, token: String) -> Result<ActionToken, Error>;
    async fn get_user_by_email(&self, email: &str) -> Result<User, Error>;
    async fn get_user_by_id(&self, id: &str) -> Result<User, Error>;
    async fn get_user_by_refresh_token(&self, refresh_token: &str) -> Result<User, Error>;
}
