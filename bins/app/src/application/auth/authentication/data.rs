use diesel::Insertable;
use serde::{Deserialize, Serialize};
use support::schema::users;
use validr::*;
#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]

pub struct LoginCredentials {
    pub email: Option<String>,
    pub password: Option<String>,
}
impl Validation for LoginCredentials {
    fn modifiers(&self) -> Vec<Modifier<Self>> {
        vec![]
    }

    fn rules(&self) -> Vec<Rule<Self>> {
        vec![
            rule_required!(email),
            rule_email!(email),
            rule_length_min!(password, 6),
            rule_required!(password),
            Rule::new(
                "password_contains_number",
                |obj: &Self, error: &mut validr::error::ValidationError| {
                    if let Some(password) = &obj.password {
                        if !password.chars().any(|c| c.is_digit(10)) {
                            error.add("password_missing_number");
                        }
                    }
                },
            ),
        ]
    }
}

// define a new struct that corresponds to the database table
#[derive(Insertable, Debug, Clone, Serialize)]
#[diesel(table_name = users)]
#[diesel(treat_none_as_null = true)]
pub struct LoginCredentialsData {
    pub email: String,
    pub password: String,
}

// implement a method to convert LoginCredentials to LoginCredentialsData
impl LoginCredentials {
    pub fn convert(self) -> LoginCredentialsData {
        let email = self.email.unwrap();
        let password = self.password.unwrap();
        LoginCredentialsData { email, password }
    }
}
