use crate::application::auth::authentication::data::LoginCredentials;

use actix_web::web;
use assert_matches::assert_matches;
use error::Error;
use validr::Validation;

#[actix_web::test]
async fn test_login_validation_with_missing_email() {
    let user_data = LoginCredentials {
        email: Some("".to_string()),
        password: Some("password".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}
#[actix_web::test]
async fn test_login_validation_with_invalid_email() {
    let user_data = LoginCredentials {
        email: Some("test".to_string()),
        password: Some("password".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}


#[actix_web::test]
async fn test_login_validation_with_correct_email() {
    let user_data = LoginCredentials {
        email: Some("test@test.com".to_string()),
        password: Some("password12".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Ok(_));
}



#[actix_web::test]
async fn test_login_validation_with_correct_password() {
    let user_data = LoginCredentials {
        email: Some("test@test.com".to_string()),
        password: Some("password12".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Ok(_));
}

#[actix_web::test]
async fn test_login_validation_with_invalid_password() {
    let user_data = LoginCredentials {
        email: Some("test@test.com".to_string()),
        password: Some("password".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}

#[actix_web::test]
async fn test_login_validation_with_missing_password() {
    let user_data = LoginCredentials {
        email: Some("test@test.com".to_string()),
        password: Some("".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}

#[actix_web::test]
async fn test_login_validation_with_short_password() {
    let user_data = LoginCredentials {
        email: Some("test@test.com".to_string()),
        password: Some("te12".to_string()),
    };
    let json_data = web::Json(user_data.clone());

    // Call the validate and convert methods on the web::Json object
    let result = json_data.into_inner().validate().map_err(Error::from);

    assert_matches!(result, Err(Error::Validation(_)));
}