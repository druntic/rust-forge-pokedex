use crate::application::auth::authentication::contract;
use actix_web::{HttpRequest, HttpResponse, web, cookie};
use error::Error;

pub async fn handle_logout<T: contract::AuthenticationContract>(
    request: HttpRequest,
    service: web::Data<T>,
) -> Result<HttpResponse, Error> {
    //get the jwt cookie from the request
    let cookie = request.cookie("jwt");

    //call the logout function with the jwt cookie
    service.logout(cookie).await?;
    //clear the jwt cookie
    let cookie = cookie::Cookie::build("jwt", "")
        .max_age(cookie::time::Duration::seconds(0))
        .path("/") //prevents multiple jwt cookies with different path
        .finish();

    Ok(HttpResponse::Ok().cookie(cookie).body("user logged out"))
}
