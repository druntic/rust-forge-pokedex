use crate::application::auth::authentication::contract;
use actix_web::{HttpResponse, cookie, web, HttpRequest};
use error::Error;
use serde_json::json;
use support::helpers::http;
use std::env;
pub async fn handle_email_verification<T: contract::AuthenticationContract>(
    service: web::Data<T>,
    request: HttpRequest,
) -> Result<HttpResponse, Error> {
    let base_url_token = http::part_from_header(&request, "token")?;
    let (access_token, refresh_token) = service.verify_email(base_url_token).await?;

    //create a cookie with the refresh token
    let refresh_token_duration = env::var("REFRESH_TOKEN_DURATION_DAYS")
    .expect("refresh token duration must be set")
    .parse::<i64>()
    .expect("REFRESH_TOKEN_DURATION_DAYS is not a valid number");
    let cookie = cookie::Cookie::build("jwt", refresh_token)
        .max_age(cookie::time::Duration::days(refresh_token_duration))
        .path("/") //prevents multiple jwt cookies with different path
        .finish();

    //create a JSON value with the access token and message
    let json = json!({
        "access_token": access_token,
        "message": "user email verified"
    });

    Ok(HttpResponse::Ok().cookie(cookie).json(json))
}
