use crate::application::auth::authentication::contract;
use crate::application::auth::authentication::data;
use actix_web::{HttpResponse, cookie, web};
use error::Error;
use serde_json::json;
use validr::Validation;
use std::env;

pub async fn handle_login<T: contract::AuthenticationContract>(
    data: web::Json<data::LoginCredentials>,
    service: web::Data<T>,
) -> Result<HttpResponse, Error> {
    let login_data = data.into_inner().validate()?.convert();
    let (access_token, refresh_token) =
        service.login(login_data.email, login_data.password).await?;
    //create a cookie with the refresh token
    let refresh_token_duration = env::var("REFRESH_TOKEN_DURATION_DAYS")
    .expect("refresh token duration must be set")
    .parse::<i64>()
    .expect("REFRESH_TOKEN_DURATION_DAYS is not a valid number");
    let cookie = cookie::Cookie::build("jwt", refresh_token)
        .max_age(cookie::time::Duration::days(refresh_token_duration))
        .path("/") //prevents multiple jwt cookies with different path
        .finish();

    //create a JSON value with the access token and message
    let json = json!({
        "access_token": access_token,
        "message": "user logged in"
    });

    Ok(HttpResponse::Ok().cookie(cookie).json(json))
}
