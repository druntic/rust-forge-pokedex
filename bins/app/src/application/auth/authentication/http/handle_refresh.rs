use crate::application::auth::authentication::contract;
use actix_web::{HttpRequest, HttpResponse, web};
use error::Error;
use serde_json::json;

pub async fn handle_refresh<T: contract::AuthenticationContract>(
    request: HttpRequest,
    service: web::Data<T>,
) -> Result<HttpResponse, Error> {
    //get the jwt cookie from the request
    let cookie = request.cookie("jwt");

    //call the refresh function with the jwt cookie
    let access_token = service.refresh(cookie).await?;
    let json = json!({
        "access_token": access_token,
    });

    Ok(HttpResponse::Ok().json(json))
}
