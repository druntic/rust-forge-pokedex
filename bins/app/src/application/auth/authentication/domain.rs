use super::contract::{AuthenticationContract, PgRepositoryContract, PgServiceContract};
use actix_web::cookie::Cookie;
use async_trait::async_trait;
use chrono::Utc;
use error::Error;
use std::env;
use support::helpers::auth;

pub struct Authentication<A: PgRepositoryContract, B: PgServiceContract> {
    pub repository: A,
    pub service: B,
}

#[async_trait]
impl<A, B> AuthenticationContract for Authentication<A, B>
where
    A: PgRepositoryContract + Sync + Send,
    B: PgServiceContract + Sync + Send,
{
    //function to verify email of user
    async fn verify_email(&self, token: String) -> Result<(String, String), Error> {
        //try to find token by token
        let action_token = self.repository.get_action_token_by_token(token).await?;

        //check if correct action name
        if action_token.action_name != "registration_verification" {
            return Err(Error::BadRequest("invalid token".to_string()));
        }

        //check if token expired
        let expires_at = action_token.expires_at.ok_or(Error::BadRequest(
            "token has no expiration date".to_string(),
        ))?;
        let time_limit = env::var("EMAIL_VERIFICATION_EXPIERY_TIME_SECONDS")
            .expect("expiery time must be set")
            .parse::<i64>()
            .expect("EMAIL_VERIFICATION_EXPIERY_TIME_SECONDS is not a valid number");
        if expires_at
            .signed_duration_since(Utc::now().naive_utc())
            .num_seconds()
            > time_limit
        {
            return Err(Error::BadRequest("link expired".to_string()));
        }

        self.service
            .update_token_action_name(&action_token.id, "executed")
            .await?;
        self.service.update_executed_at(&action_token.id).await?;
        self.service
            .update_email_verified_at(&action_token.entity_id)
            .await?;

        //create jwt access and refresh tokens
        let access_token_string = auth::create_access_token(&action_token.entity_id).await?;
        let refresh_token_string = auth::create_refresh_token(&action_token.entity_id).await?;
        self.service
            .update_refresh_token(
                action_token.entity_id.as_str(),
                refresh_token_string.as_str(),
            )
            .await?;
        Ok((access_token_string, refresh_token_string))
    }
    //function to login user
    async fn login(&self, email: String, password: String) -> Result<(String, String), Error> {
        //compare password with user password
        let user = self.repository.get_user_by_email(email.as_str()).await?;
        let user_password = user.password.expect("something went wrong");
        let password_is_valid = auth::verify_password(&password, &user_password)?;

        if !password_is_valid {
            return Err(Error::Unauthorized("invalid credentials".to_string()));
        }

        if user.email_verified_at.is_none() {
            return Err(Error::Unauthorized("email not verified".to_string()));
        }
        //continue creating access and refresh tokens
        let access_token_string = auth::create_access_token(&user.id).await?;
        let refresh_token_string = auth::create_refresh_token(&user.id).await?;
        self.service
            .update_refresh_token(user.id.as_str(), refresh_token_string.as_str())
            .await?;
        Ok((access_token_string, refresh_token_string))
    }

    async fn logout(&self, cookie: Option<Cookie<'_>>) -> Result<usize, Error> {
        let refresh_token_string;
        //check if jwt cookie exists
        if let Some(cookie) = cookie {
            refresh_token_string = cookie.value().to_string();
        } else {
            return Err(Error::Unauthorized("user not logged in".to_string()));
        }
        let user = self
            .repository
            .get_user_by_refresh_token(&refresh_token_string)
            .await?;
        //check if the refresh token in cookies matches the refresh token for the found user
        if Some(refresh_token_string) != user.refresh_token {
            return Err(Error::Unauthorized(
                "user does not match for refresh token".to_string(),
            ));
        }
        //delete the users refresh token in the database
        self.service
            .update_refresh_token(user.id.as_str(), "")
            .await
    }

    async fn refresh(&self, cookie: Option<Cookie<'_>>) -> Result<String, Error> {
        let refresh_token_string;
        //check if jwt cookie exists
        if let Some(cookie) = cookie {
            refresh_token_string = cookie.value().to_string();
        } else {
            return Err(Error::Unauthorized("user not logged in".to_string()));
        }
        let user = self
            .repository
            .get_user_by_refresh_token(&refresh_token_string)
            .await?;
        //check if the refresh token in cookies matches the refresh token for the found user
        if Some(refresh_token_string) != user.refresh_token {
            return Err(Error::Unauthorized(
                "user does not match for refresh token".to_string(),
            ));
        }
        //create a new access token
        auth::create_access_token(user.id.as_str()).await
    }
}
