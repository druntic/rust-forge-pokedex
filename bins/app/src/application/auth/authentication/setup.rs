use crate::application::auth::authentication::domain::Authentication;
use crate::application::auth::authentication::http;
use crate::application::auth::authentication::infrastructure::{PgRepository, PgService};
use actix_web::web;

pub fn routes(cfg: &mut web::ServiceConfig) {
    let service = Authentication {
        service: PgService {},
        repository: PgRepository {},
    };
    cfg.app_data(web::Data::new(service));

    cfg.route(
        "/auth/verify-email/{token}",
        web::get().to(
            http::handle_email_verification::handle_email_verification::<
                Authentication<PgRepository, PgService>,
            >,
        ),
    );

    cfg.route(
        "/auth/login",
        web::post().to(http::handle_login::handle_login::<Authentication<PgRepository, PgService>>),
    );

    cfg.route(
        "/auth/logout",
        web::post()
            .to(http::handle_logout::handle_logout::<Authentication<PgRepository, PgService>>),
    );

    cfg.route(
        "/auth/refresh",
        web::get()
            .to(http::handle_refresh::handle_refresh::<Authentication<PgRepository, PgService>>),
    );
}
