use crate::application::auth::authentication::contract::{PgRepositoryContract, PgServiceContract};
use async_trait::async_trait;
use error::Error;

use support::store::models::action_token;
use support::store::models::user::User;

pub struct PgService {}

pub struct PgRepository {}
#[async_trait]
impl PgServiceContract for PgService {
    async fn update_token_action_name(
        &self,
        token_id: &str,
        action_name: &str,
    ) -> Result<usize, Error> {
        action_token::update_token_action_name(token_id, action_name).await
    }

    async fn update_executed_at(&self, id: &str) -> Result<usize, Error> {
        action_token::update_executed_at(id).await
    }

    async fn update_email_verified_at(&self, id: &str) -> Result<usize, Error> {
        User::update_email_verified_at(id).await
    }

    async fn update_refresh_token(&self, id: &str, refresh_token: &str) -> Result<usize, Error> {
        User::update_refresh_token(id, refresh_token).await
    }
}

#[async_trait]
impl PgRepositoryContract for PgRepository {
    async fn exists(&self, email: &str) -> Result<User, Error> {
        let users = User::exists(email).await?;
        if !users.is_empty() {
            return Ok(users[0].clone());
        } else {
            return Err(Error::BadRequest("something went wrong".to_string()));
        }
    }

    async fn get_action_token_by_token(
        &self,
        token: String,
    ) -> Result<action_token::ActionToken, Error> {
        action_token::get_action_token_by_token(token).await
    }

    async fn get_user_by_email(&self, email: &str) -> Result<User, Error> {
        User::get_user_by_email(email).await
    }

    async fn get_user_by_id(&self, id: &str) -> Result<User, Error> {
        User::get_user_by_id(id).await
    }

    async fn get_user_by_refresh_token(&self,refresh_token: &str) -> Result<User, Error>{
        User::get_user_by_refresh_token(refresh_token).await
    }
}
