use actix_web::{web, HttpResponse};
pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.route(
        "/_health",
        web::get().to(|| async { HttpResponse::Ok().body("I'm healthy".to_string()) }),
    );

    registration(cfg);
    authentication(cfg);
}

fn registration(c: &mut web::ServiceConfig) {
    crate::application::auth::registration::setup::routes(c);
}

fn authentication(c: &mut web::ServiceConfig) {
    crate::application::auth::authentication::setup::routes(c);
}
