pub mod application;
pub mod web;
use dotenvy::dotenv;
pub mod middleware;

#[actix_web::main]
async fn main() {
    dotenv().ok();
    support::services::seeder::run().await;
    web::start_server().await.expect("something went wrong");
}
