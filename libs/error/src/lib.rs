
use actix_web::{error::InternalError, HttpResponse, ResponseError};
use bcrypt::BcryptError;
use crypto_common::InvalidLength;
use diesel::result::Error as DieselError;
use diesel::ConnectionError;
use jwt::error::Error as JwtError;
use lettre::address::AddressError;
use lettre::error::Error as LettreBodyError;
use lettre::transport::smtp::Error as LettreError;
use reqwest::Error as ReqwestError;
use std::{error::Error as StdError, fmt, io::Error as IoError, str};
use validr::error::ValidationErrors;
use chrono::ParseError;

#[allow(clippy::enum_variant_names)]
#[derive(Debug)]
pub enum Error {
    // storage / file upload error
    NotFound,
    Diesel(DieselError),
    Validation(ValidationErrors),
    Reqwest(ReqwestError),
    NotFoundWithCause(String),
    InternalError(String),
    Utf8(str::Utf8Error),
    Connection(ConnectionError),
    Io(String),
    BadRequest(String),
    Bcrypt(BcryptError),
    Lettre(LettreError),
    Address(AddressError),
    LettreBody(LettreBodyError),
    Jwt(JwtError),
    Hmac(InvalidLength),
    Chrono(ParseError),
    Unauthorized(String),

}

// Allow the use of "{}" format specifier
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::NotFound => write!(f, "Not Found"),
            Error::Diesel(ref cause) => write!(f, "DB Error: {}", cause),
            Error::Validation(ref cause) => {
                write!(f, "Validation error: {}", cause)
            }
            Error::Reqwest(ref cause) => {
                write!(f, "Reqwest error: {}", cause)
            }
            Error::NotFoundWithCause(ref cause) => write!(f, "Not found: {}", cause),
            Error::InternalError(ref cause) => write!(f, "{}", cause),
            Error::Utf8(ref cause) => write!(f, "UTF-8 error: {}", cause),
            Error::Connection(ref cause) => write!(f, "Connection error: {}", cause),
            Error::Io(ref cause) => write!(f, "Io Error: {}", cause),
            Error::BadRequest(ref cause) => write!(f, "Bad request: {}", cause),
            Error::Bcrypt(ref cause) => write!(f, "Bcrypt error: {}", cause),
            Error::Lettre(ref cause) => write!(f, "Lettre error: {}", cause),
            Error::Address(ref cause) => write!(f, "Address error: {}", cause),
            Error::LettreBody(ref cause) => write!(f, "Lettre body error: {}", cause),
            Error::Jwt(ref cause) => write!(f, "Jwt error: {}", cause),
            Error::Hmac(ref cause) => write!(f, "Hmac error: {}", cause),
            Error::Chrono(ref cause) => write!(f, "Chrono error: {}", cause),
            Error::Unauthorized(ref cause) => write!(f, "Unauthorized error: {}", cause),
            
        }
    }
}

impl StdError for Error {
    fn cause(&self) -> Option<&dyn StdError> {
        match *self {
            Error::NotFound => None,
            Error::Diesel(ref cause) => Some(cause),
            Error::Validation(ref cause) => Some(cause),
            Error::Reqwest(ref cause) => Some(cause),
            Error::NotFoundWithCause(ref _cause) => None,
            Error::InternalError(ref _cause) => None,
            Error::Utf8(ref cause) => Some(cause),
            Error::Connection(ref cause) => Some(cause),
            Error::Io(ref _cause) => None,
            Error::BadRequest(ref _cause) => None,
            Error::Bcrypt(ref cause) => Some(cause),
            Error::Lettre(ref cause) => Some(cause),
            Error::Address(ref cause) => Some(cause),
            Error::LettreBody(ref cause) => Some(cause),
            Error::Jwt(ref cause) => Some(cause),
            Error::Hmac(ref cause) => Some(cause),
            Error::Chrono(ref cause) => Some(cause),
            Error::Unauthorized(ref _cause) => None,
            
        }
    }
}

impl From<DieselError> for Error {
    fn from(cause: DieselError) -> Error {
        if cause == DieselError::NotFound {
            return Error::NotFound;
        }
        Error::Diesel(cause)
    }
}

impl From<ValidationErrors> for Error {
    fn from(cause: ValidationErrors) -> Error {
        Error::Validation(cause)
    }
}

impl From<ReqwestError> for Error {
    // Implement the From trait for reqwest errors
    fn from(cause: ReqwestError) -> Error {
        Error::Reqwest(cause)
    }
}

impl From<InternalError<ReqwestError>> for Error {
    fn from(cause: InternalError<ReqwestError>) -> Error {
        Error::InternalError(cause.to_string())
    }
}

impl From<std::str::Utf8Error> for Error {
    fn from(cause: std::str::Utf8Error) -> Error {
        Error::Utf8(cause)
    }
}

impl From<ConnectionError> for Error {
    fn from(cause: ConnectionError) -> Error {
        Error::Connection(cause)
    }
}

impl From<IoError> for Error {
    fn from(cause: IoError) -> Error {
        Error::Io(format!("{:?}", cause))
    }
}

impl From<BcryptError> for Error {
    // define how to convert a BcryptError into an Error
    fn from(error: BcryptError) -> Self {
        // use the InternalError variant to wrap the BcryptError message
        Error::InternalError(error.to_string())
    }
}

impl From<LettreError> for Error {
    fn from(cause: LettreError) -> Error {
        Error::Lettre(cause) // return the new variant
    }
}
impl From<AddressError> for Error {
    fn from(cause: AddressError) -> Error {
        Error::Address(cause) // return the address variant
    }
}
// Implement the From trait for the lettre body error
impl From<LettreBodyError> for Error {
    fn from(cause: LettreBodyError) -> Error {
        Error::LettreBody(cause) // return the lettre body variant
    }
}

impl From<JwtError> for Error {
    fn from(e: JwtError) -> Error {
        Error::Jwt(e)
    }
}

impl From<InvalidLength> for Error {
    fn from(e: InvalidLength) -> Error {
        Error::Hmac(e)
    }
}

impl From<chrono::ParseError> for Error {
    fn from(error: chrono::ParseError) -> Self {
        Error::Chrono(error)
    }
}


#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct ErrorBody {
    pub message: Option<String>,
    pub code: String,
    pub cause: Option<String>,
    pub payload: Option<serde_json::Value>,
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        let mut response = match self {
            Error::NotFound => HttpResponse::NotFound(),
            Error::NotFoundWithCause(_cause) => HttpResponse::NotFound(),
            Error::InternalError(_cause) => HttpResponse::InternalServerError(),
            Error::BadRequest(_) => HttpResponse::BadRequest(),
            _ => HttpResponse::InternalServerError(),
        };

        response.json(self.into_error_body())
    }
}


impl Error {
    pub fn into_error_body(&self) -> ErrorBody {
        match *self {
            Error::Diesel(ref cause) => ErrorBody {
                message: Some("DB Error".to_string()),
                code: "db".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::NotFound => ErrorBody {
                message: Some("Entity not found".to_string()),
                code: 404.to_string(),
                cause: None,
                payload: None,
            },

            Error::Validation(ref _cause) => ErrorBody {
                message: Some("Invalid body".to_string()),
                code: "validation".to_string(),
                cause: None,
                payload: None,
            },
            Error::Reqwest(ref cause) => ErrorBody {
                message: Some("Reqwest error".to_string()),
                code: "reqwest".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::NotFoundWithCause(ref cause) => ErrorBody {
                message: Some("Entity not found".to_string()),
                code: "not_found".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::InternalError(ref cause) => ErrorBody {
                message: Some("Internal server error".to_string()),
                code: "server".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Utf8(ref cause) => ErrorBody {
                message: Some("UTF-8 error".to_string()),
                code: "utf8".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Connection(ref cause) => ErrorBody {
                message: Some("Connection error".to_string()),
                code: "connection".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Io(ref cause) => ErrorBody {
                message: Some("Io Error".to_string()),
                code: "io".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::BadRequest(ref cause) => ErrorBody {
                message: Some("Bad request".to_string()),
                code: "general".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Bcrypt(ref cause) => ErrorBody {
                message: Some("Bcrypt error".to_string()),
                code: "bcrypt".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Lettre(ref cause) => ErrorBody {
                message: Some("Lettre error".to_string()),
                code: "lettre".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Address(ref cause) => ErrorBody {
                message: Some("Address error".to_string()),
                code: "address".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::LettreBody(ref cause) => ErrorBody {
                message: Some("Lettre body error".to_string()),
                code: "address".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Jwt(ref cause) => ErrorBody {
                message: Some("Jwt error".to_string()),
                code: "jwt".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },
            Error::Hmac(ref cause) => ErrorBody {
                message: Some("Hmac error".to_string()),
                code: "hmac".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },

            Error::Chrono(ref cause) => ErrorBody {
                message: Some("Chrono error".to_string()),
                code: "chrono".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },

            Error::Unauthorized(ref cause) => ErrorBody {
                message: Some("Unauthorized error".to_string()),
                code: "unauthorized".to_string(),
                cause: Some(cause.to_string()),
                payload: None,
            },

        }
    }
}
