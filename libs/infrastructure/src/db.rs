use diesel::pg::PgConnection;
use diesel::prelude::*;
use error::Error;
use std::{env, process::Command};

pub fn establish_connection() -> Result<PgConnection, ConnectionError> {
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    PgConnection::establish(&database_url)
}

pub fn establish_connection_test() -> Result<PgConnection, ConnectionError> {
    let database_url = env::var("DATABASE_TEST_URL").expect("DATABASE_TEST_URL must be set");
    PgConnection::establish(&database_url)
}

pub fn setup_test_database() -> Result<(), Error> {
    let mut command = Command::new("diesel");
    let db_url = env::var("DATABASE_TEST_URL").expect("DATABASE_TEST_URL must be set");
    command.args(["setup", "--database-url", &db_url]);
    let status = command.status().map_err(Error::from)?;

    // Check if the command was successful
    if status.success() {
        println!("Database setup completed");
        Ok(())
    } else {
        Err(Error::InternalError("Command failed".to_string()))
    }
}
