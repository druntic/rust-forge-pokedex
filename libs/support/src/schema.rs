// @generated automatically by Diesel CLI.

diesel::table! {
    action_tokens (id) {
        #[max_length = 36]
        id -> Varchar,
        entity_id -> Varchar,
        token -> Varchar,
        action_name -> Varchar,
        executed_at -> Nullable<Timestamptz>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
        expires_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    pokemon_abilities (id) {
        #[max_length = 36]
        id -> Varchar,
        name -> Nullable<Varchar>,
        #[max_length = 36]
        pokemon_id -> Nullable<Varchar>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    pokemon_catches (id) {
        #[max_length = 36]
        id -> Varchar,
        #[max_length = 36]
        user_id -> Nullable<Varchar>,
        #[max_length = 36]
        pokemon_id -> Nullable<Varchar>,
        is_success -> Nullable<Bool>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    pokemon_stats (id) {
        #[max_length = 36]
        id -> Varchar,
        #[max_length = 36]
        pokemon_id -> Nullable<Varchar>,
        name -> Nullable<Varchar>,
        base_stat -> Nullable<Int4>,
        effort -> Nullable<Int4>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    pokemon_types (id) {
        #[max_length = 36]
        id -> Varchar,
        #[max_length = 36]
        pokemon_id -> Nullable<Varchar>,
        name -> Nullable<Varchar>,
        slot -> Nullable<Int4>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    pokemons (id) {
        #[max_length = 36]
        id -> Varchar,
        name -> Nullable<Varchar>,
        base_experience -> Nullable<Int4>,
        height -> Nullable<Int4>,
        pokemon_id -> Nullable<Int4>,
        is_default -> Nullable<Bool>,
        pokemon_order -> Nullable<Int4>,
        image -> Nullable<Varchar>,
        weight -> Nullable<Int4>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    user_pokedexes (id) {
        #[max_length = 36]
        id -> Varchar,
        #[max_length = 36]
        user_id -> Nullable<Varchar>,
        #[max_length = 36]
        pokemon_id -> Nullable<Varchar>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
    }
}

diesel::table! {
    users (id) {
        #[max_length = 36]
        id -> Varchar,
        email -> Nullable<Varchar>,
        first_name -> Nullable<Varchar>,
        last_name -> Nullable<Varchar>,
        password -> Nullable<Varchar>,
        refresh_token -> Nullable<Varchar>,
        created_at -> Nullable<Timestamptz>,
        updated_at -> Nullable<Timestamptz>,
        email_verified_at -> Nullable<Timestamptz>,
    }
}

diesel::joinable!(pokemon_abilities -> pokemons (pokemon_id));
diesel::joinable!(pokemon_catches -> pokemons (pokemon_id));
diesel::joinable!(pokemon_catches -> users (user_id));
diesel::joinable!(pokemon_stats -> pokemons (pokemon_id));
diesel::joinable!(pokemon_types -> pokemons (pokemon_id));
diesel::joinable!(user_pokedexes -> pokemons (pokemon_id));
diesel::joinable!(user_pokedexes -> users (user_id));

diesel::allow_tables_to_appear_in_same_query!(
    action_tokens,
    pokemon_abilities,
    pokemon_catches,
    pokemon_stats,
    pokemon_types,
    pokemons,
    user_pokedexes,
    users,
);
