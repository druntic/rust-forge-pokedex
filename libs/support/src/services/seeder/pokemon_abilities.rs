use crate::store::models::pokemon::{Pokemon, PokemonAbilityData};
use diesel::prelude::*;
use error::Error;
use serde_json::Value;
use std::env;


pub async fn seed_pokemon_ability(
    pokemon_id: i32,
    connection: &mut PgConnection,
) -> Result<(), Error> {
    let pokemon_url = env::var("POKEMON_URL").expect("invalid pokemon url");
    let url = format!("{}/{}", pokemon_url, pokemon_id);
    // get the response body from the url
    let body = reqwest::get(url).await?.text().await?;
    create_pokemon_ability(&body, connection, pokemon_id).await
}

pub async fn create_pokemon_ability(
    body: &str,
    connection: &mut PgConnection,
    pokemon_id: i32,
) -> Result<(), Error> {
    //find pokemon in database using pokemon_id and get his id
    let json: Value = serde_json::from_str(body).unwrap_or(Value::Null);
    let result = Pokemon::get_pokemon_by_pokemon_id(pokemon_id, connection)?;

    let empty_vec = vec![];
    //get array of abilities
    let abilities_array = json["abilities"].as_array().unwrap_or(&empty_vec);

    //for each ability in abilities array insert new value in pokemon ability table
    for pokemon_ability in abilities_array {
        let new_ability = PokemonAbilityData {
            name: pokemon_ability["ability"]["name"]
                .as_str()
                .map(|s| s.to_string()),
            pokemon_id: Some(result.id.clone()),
        };

        new_ability.save(connection)?;
    }

    Ok(())
}
