pub mod pokemon;
pub mod pokemon_abilities;
pub mod pokemon_stats;
pub mod pokemon_types;

pub async fn run() {
    println!("Starting seeding process...");
    match infrastructure::db::establish_connection() {
        Ok(mut connection) => match pokemon::seed_pokemons(&mut connection).await {
            Ok(_) => println!("Seeding completed."),
            Err(error) => println!("Error while seeding pokemons: {}", error),
        },
        Err(error) => println!("Error while establishing connection: {}", error),
    }
}

#[cfg(test)]
mod test;
