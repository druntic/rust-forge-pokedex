use crate::store::models::pokemon::{Pokemon, PokemonAbility, PokemonData, PokemonStat, PokemonType};
use crate::services::seeder::{pokemon_abilities, pokemon_stats, pokemon_types};
use diesel::prelude::*;
use error::Error;
use serde_json::Value;
use std::env;

pub async fn seed_pokemons(connection: &mut PgConnection) -> Result<(), Error> {
    let pokemon_url = env::var("POKEMON_URL").expect("invalid pokemon url");

    // use a for loop to iterate over the range of values from 1 to 151
    for i in 1..=151 {
        // query the database for the pokemon with pokemon_id=i
        let result = Pokemon::get_pokemon_by_pokemon_id(i, connection);
        match result {
            //if pokemon already exists check if we need to seed stats, types or abilities for that pokemon
            Ok(pokemon) => {
                let abilities =
                    PokemonAbility::get_pokemon_abilities_by_id(&pokemon.id, connection)?;
                if abilities.is_empty() {
                    pokemon_abilities::seed_pokemon_ability(i, connection).await?;
                }
                let stats = PokemonStat::get_pokemon_stats_by_id(&pokemon.id, connection)?;
                if stats.is_empty() {
                    pokemon_stats::seed_pokemon_stats(i, connection).await?;
                }
                let types = PokemonType::get_pokemon_types_by_id(&pokemon.id, connection)?;
                if types.is_empty() {
                    pokemon_types::seed_pokemon_types(i, connection).await?;
                }
            }
            //if we cant find the pokemon we first create it and then create its stats, abilities, types
            Err(_) => {
                let url = format!("{}/{}", pokemon_url, i);
                // get the response body from the url
                let body = reqwest::get(url).await?.text().await?;
                create_pokemon(&body, connection).await?;
                pokemon_types::create_pokemon_types(&body, connection, i).await?;
                pokemon_stats::create_pokemon_stats(&body, connection, i).await?;
                pokemon_abilities::create_pokemon_ability(&body, connection, i).await?;
            }
        }
    }
    println!("seeded pokemons");
    Ok(())
}
pub async fn create_pokemon(body: &str, connection: &mut PgConnection) -> Result<usize, Error> {
    // parse the body as a JSON value
    let json: Value = serde_json::from_str(body).unwrap_or(Value::Null);

    // create a new pokemon struct with the data
    let new_pokemon = PokemonData {
        name: json["forms"][0]["name"].as_str().map(|s| s.to_string()),
        base_experience: json["base_experience"].as_u64().map(|n| n as i32),
        height: json["height"].as_i64().map(|n| n as i32),
        pokemon_id: json["id"].as_i64().map(|n| n as i32),
        is_default: json["is_default"].as_bool(),
        pokemon_order: json["order"].as_i64().map(|n| n as i32),
        image: json["sprites"]["front_default"]
            .as_str()
            .map(|s| s.to_string()),
        weight: json["weight"].as_i64().map(|n| n as i32),
    };
    println!("created pokemon {:?}", new_pokemon.pokemon_id);

    // Call the save function on the new_pokemon and handle the result
    new_pokemon.save(connection)
}
