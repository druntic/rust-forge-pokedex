use crate::store::models::pokemon::{Pokemon, PokemonTypeData};
use diesel::prelude::*;
use error::Error;
use serde_json::Value;
use std::env;

pub async fn seed_pokemon_types(
    pokemon_id: i32,
    connection: &mut PgConnection,
) -> Result<(), Error> {
    let pokemon_url = env::var("POKEMON_URL").expect("invalid pokemon url");
    let url = format!("{}/{}", pokemon_url, pokemon_id);
    // get the response body from the url
    let body = reqwest::get(url).await?.text().await?;
    create_pokemon_types(&body, connection, pokemon_id).await
}

pub async fn create_pokemon_types(
    body: &str,
    connection: &mut PgConnection,
    pokemon_id: i32,
) -> Result<(), Error> {
    //find pokemon in database using pokemon_id and get his id
    let json: Value = serde_json::from_str(body).unwrap_or(Value::Null);
    let result = Pokemon::get_pokemon_by_pokemon_id(pokemon_id, connection)?;

    let empty_vec = vec![];
    //get array of types
    let types_array = json["types"].as_array().unwrap_or(&empty_vec);

    //for each type in types array insert new value in pokemon types table
    for pokemon_type in types_array {
        let new_type = PokemonTypeData {
            name: pokemon_type["type"]["name"].as_str().map(|s| s.to_string()),
            pokemon_id: Some(result.id.clone()),
            slot: pokemon_type["slot"].as_i64().map(|n| n as i32),
        };

        new_type.save(connection)?;
    }

    Ok(())
}
