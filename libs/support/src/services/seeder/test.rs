use crate::store::models::pokemon::{Pokemon, PokemonAbility, PokemonStat, PokemonType};
use crate::schema::{pokemon_abilities, pokemon_stats, pokemon_types, pokemons};
use crate::services::seeder;
use diesel::prelude::*;
use dotenvy::dotenv;
use serial_test::serial;

#[actix_web::test]
#[serial]
async fn test_seed_pokemons() {
    dotenv().ok();
    let setup_res = infrastructure::db::setup_test_database();
    assert!(setup_res.is_ok());
    let connection = &mut infrastructure::db::establish_connection_test().unwrap();
    let res = seeder::pokemon::seed_pokemons(connection).await;
    assert!(res.is_ok());
    let pokemon_id: i32 = 1;
    let pokemon = pokemons::table
        .filter(pokemons::pokemon_id.eq(pokemon_id))
        .first::<Pokemon>(connection)
        .expect("pokemon not found");
    assert_eq!("bulbasaur", pokemon.name.unwrap());

    let pokemon_ability = pokemon_abilities::table
        .filter(pokemon_abilities::pokemon_id.eq(&pokemon.id))
        .first::<PokemonAbility>(connection)
        .expect("ability not found");
    assert_eq!("overgrow", pokemon_ability.name.unwrap());

    let stat = pokemon_stats::table
        .filter(pokemon_stats::pokemon_id.eq(&pokemon.id))
        .offset(2) //get third pokemon stat(defense)
        .get_result::<PokemonStat>(connection)
        .expect("stat not found");
    assert_eq!(49, stat.base_stat.unwrap());

    let pokemon_type = pokemon_types::table
        .filter(pokemon_types::pokemon_id.eq(&pokemon.id))
        .first::<PokemonType>(connection)
        .expect("type not found");

    assert_eq!("grass", pokemon_type.name.unwrap());
}
