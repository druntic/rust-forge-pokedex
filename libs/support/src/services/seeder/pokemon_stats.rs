use crate::store::models::pokemon::{Pokemon, PokemonStatData};
use diesel::prelude::*;
use error::Error;
use serde_json::Value;
use std::env;

pub async fn seed_pokemon_stats(
    pokemon_id: i32,
    connection: &mut PgConnection,
) -> Result<(), Error> {
    let pokemon_url = env::var("POKEMON_URL").expect("invalid pokemon url");
    let url = format!("{}/{}", pokemon_url, pokemon_id);
    // get the response body from the url
    let body = reqwest::get(url).await?.text().await?;
    create_pokemon_stats(&body, connection, pokemon_id).await
}

pub async fn create_pokemon_stats(
    body: &str,
    connection: &mut PgConnection,
    pokemon_id: i32,
) -> Result<(), Error> {
    //find pokemon in database using pokemon_id and get his id
    let json: Value = serde_json::from_str(body).unwrap_or(Value::Null);
    let result = Pokemon::get_pokemon_by_pokemon_id(pokemon_id, connection)?;

    let empty_vec = vec![];
    //get array of 6 stats
    let stats_array = json["stats"].as_array().unwrap_or(&empty_vec);

    for stat in stats_array {
        //create new stat and save to db
        let new_stat = PokemonStatData {
            name: stat["stat"]["name"].as_str().map(|s| s.to_string()),
            pokemon_id: Some(result.id.clone()),
            base_stat: stat["base_stat"].as_i64().map(|n| n as i32),
            effort: stat["effort"].as_u64().map(|n| n as i32),
        };

        new_stat.save(connection)?;
    }
    Ok(())
}
