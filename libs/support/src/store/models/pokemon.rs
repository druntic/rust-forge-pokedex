use crate::schema::{pokemon_abilities, pokemon_stats, pokemon_types, pokemons};
use chrono::naive::NaiveDateTime;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use error::Error;
use serde::{Deserialize, Serialize};

#[derive(Queryable, Selectable, Insertable, Debug, Clone, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::pokemons)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct Pokemon {
    pub id: String,
    pub name: Option<String>,
    pub base_experience: Option<i32>,
    pub height: Option<i32>,
    pub pokemon_id: Option<i32>,
    pub is_default: Option<bool>,
    pub pokemon_order: Option<i32>,
    pub image: Option<String>,
    pub weight: Option<i32>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
}

impl Pokemon {
    pub fn get_pokemon_by_pokemon_id(
        pokemon_id: i32,
        connection: &mut PgConnection,
    ) -> Result<Pokemon, Error> {
        pokemons::table
            .filter(pokemons::pokemon_id.eq(pokemon_id))
            .first::<Pokemon>(connection)
            .map_err(Error::from)
    }

    pub fn get_pokemon_by_id(id: String, connection: &mut PgConnection) -> Result<Pokemon, Error> {
        pokemons::table
            .filter(pokemons::id.eq(id))
            .first::<Pokemon>(connection)
            .map_err(Error::from)
    }
}
#[derive(Queryable, Selectable, Insertable, Debug, Clone, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::pokemons)]
pub struct PokemonData {
    pub name: Option<String>,
    pub base_experience: Option<i32>,
    pub height: Option<i32>,
    pub pokemon_id: Option<i32>,
    pub is_default: Option<bool>,
    pub pokemon_order: Option<i32>,
    pub image: Option<String>,
    pub weight: Option<i32>,
}

impl PokemonData {
    pub fn save(&self, connection: &mut PgConnection) -> Result<usize, Error> {
        // Use diesel to insert the values into the pokemons table and execute the query
        diesel::insert_into(pokemons::table)
            .values(self)
            .on_conflict(pokemons::name) // specify the name column as the conflict target
            .do_nothing() // do nothing if the name already exists
            .execute(connection) // execute the query and return the number of rows affected
            .map_err(Error::from) // convert any diesel error to your custom error type
    }
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug)]
#[diesel(table_name = crate::schema::pokemon_abilities)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct PokemonAbility {
    pub id: String,
    pub name: Option<String>,
    pub pokemon_id: Option<String>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
}

impl PokemonAbility {
    pub fn get_pokemon_abilities_by_id(
        id: &str,
        connection: &mut PgConnection,
    ) -> Result<Vec<PokemonAbility>, Error> {
        pokemon_abilities::table
            .filter(pokemon_abilities::pokemon_id.eq(&id))
            .load::<PokemonAbility>(connection)
            .map_err(Error::from)
    }
}

#[derive(Queryable, Selectable, Insertable, Deserialize, Serialize, Debug)]
#[diesel(table_name = crate::schema::pokemon_abilities)]
pub struct PokemonAbilityData {
    pub name: Option<String>,
    pub pokemon_id: Option<String>,
}

impl PokemonAbilityData {
    pub fn save(&self, connection: &mut PgConnection) -> Result<usize, Error> {
        // Use diesel to insert the values into the pokemons table and execute the query
        diesel::insert_into(pokemon_abilities::table)
            .values(self)
            .execute(connection)
            .map_err(Error::from)
    }
}

#[derive(Queryable, Selectable, Insertable, Debug, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::pokemon_stats)]
pub struct PokemonStat {
    pub id: String,
    pub pokemon_id: Option<String>,
    pub name: Option<String>,
    pub base_stat: Option<i32>,
    pub effort: Option<i32>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
}

impl PokemonStat {
    pub fn get_pokemon_stats_by_id(
        id: &str,
        connection: &mut PgConnection,
    ) -> Result<Vec<PokemonStat>, Error> {
        pokemon_stats::table
            .filter(pokemon_stats::pokemon_id.eq(id))
            .load::<PokemonStat>(connection)
            .map_err(Error::from)
    }
}

#[derive(Queryable, Selectable, Insertable, Debug, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::pokemon_stats)]
pub struct PokemonStatData {
    pub name: Option<String>,
    pub pokemon_id: Option<String>,
    pub base_stat: Option<i32>,
    pub effort: Option<i32>,
}

impl PokemonStatData {
    pub fn save(&self, connection: &mut PgConnection) -> Result<usize, Error> {
        // Use diesel to insert the values into the pokemons table and execute the query
        diesel::insert_into(pokemon_stats::table)
            .values(self)
            .execute(connection)
            .map_err(Error::from)
    }
}

#[derive(Queryable, Selectable, Insertable, Debug, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::pokemon_types)]
pub struct PokemonType {
    pub id: String,
    pub pokemon_id: Option<String>,
    pub name: Option<String>,
    pub slot: Option<i32>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
}

impl PokemonType {
    pub fn get_pokemon_types_by_id(
        id: &str,
        connection: &mut PgConnection,
    ) -> Result<Vec<PokemonType>, Error> {
        pokemon_types::table
            .filter(pokemon_types::pokemon_id.eq(id))
            .load::<PokemonType>(connection)
            .map_err(Error::from)
    }
}

#[derive(Queryable, Selectable, Insertable, Debug, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::pokemon_types)]
pub struct PokemonTypeData {
    pub name: Option<String>,
    pub pokemon_id: Option<String>,
    pub slot: Option<i32>,
}

impl PokemonTypeData {
    pub fn save(&self, connection: &mut PgConnection) -> Result<usize, Error> {
        // Use diesel to insert the values into the pokemons table and execute the query
        diesel::insert_into(pokemon_types::table)
            .values(self)
            .execute(connection)
            .map_err(Error::from)
    }
}
