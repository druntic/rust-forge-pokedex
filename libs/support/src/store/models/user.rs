use crate::helpers::auth;
use crate::schema::users;
use chrono::naive::NaiveDateTime;
use diesel::prelude::*;
use error::Error;
use serde::{Deserialize, Serialize};
#[derive(Queryable, Selectable, Insertable, Debug, Clone, Deserialize, Serialize)]
#[diesel(table_name = crate::schema::users)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct User {
    pub id: String,
    pub email: Option<String>,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub password: Option<String>,
    pub refresh_token: Option<String>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
    pub email_verified_at: Option<NaiveDateTime>,
}

impl User {
    //get the user by user email
    pub async fn get_user_by_email(email: &str) -> Result<User, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        users::table
            .filter(users::email.eq(email))
            .first::<User>(&mut connection)
            .map_err(Error::from)
    }
    // get the user by user id
    pub async fn get_user_by_id(id: &str) -> Result<User, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        users::table
            .filter(users::id.eq(id))
            .first::<User>(&mut connection)
            .map_err(Error::from)
    }
    //update users email_verified_at attribute to the current time
    pub async fn update_email_verified_at(id: &str) -> Result<usize, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        let now = chrono::Local::now().naive_local();
        diesel::update(users::table.filter(users::id.eq(id)))
            .set(users::email_verified_at.eq(now))
            .execute(&mut connection)
            .map_err(Error::from)
    }
    //get vector of users
    pub async fn exists(email: &str) -> Result<Vec<User>, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        users::table
            .filter(users::email.eq(email))
            .load::<User>(&mut connection)
            .map_err(Error::from)
    }
    //find user by id and update his refresh token
    pub async fn update_refresh_token(id: &str, refresh_token: &str) -> Result<usize, Error> {
        let mut connection = infrastructure::db::establish_connection()?;
        diesel::update(users::table.filter(users::id.eq(id)))
            .set(users::refresh_token.eq(refresh_token))
            .execute(&mut connection)
            .map_err(Error::from)
    }

    //get user by refresh token
    pub async fn get_user_by_refresh_token(refresh_token: &str) -> Result<User, Error> {
        let user_id = auth::verify_refresh_token(refresh_token).await?;
        Self::get_user_by_id(user_id.as_str()).await
    }
}
