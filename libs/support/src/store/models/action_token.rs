use crate::schema::action_tokens;
use chrono::naive::NaiveDateTime;
use diesel::prelude::*;
use error::Error;
use serde::Deserialize;

#[derive(Queryable, Debug, Clone, Deserialize)]
#[diesel(table_name = action_tokens)]
pub struct ActionToken {
    pub id: String,
    pub entity_id: String,
    pub token: String,
    pub action_name: String,
    pub executed_at: Option<NaiveDateTime>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
    pub expires_at: Option<NaiveDateTime>,
}
//get token by id and update his action name
pub async fn update_token_action_name(
    token_id: &str,
    action_name: &str,
) -> Result<usize, Error> {
    let mut connection = infrastructure::db::establish_connection()?;
    // Update the action name
    diesel::update(action_tokens::table.filter(action_tokens::id.eq(token_id)))
        .set(action_tokens::action_name.eq(action_name))
        .execute(&mut connection)
        .map_err(Error::from)
}
//get token by id and update his executed_at attribute
pub async fn update_executed_at(id: &str) -> Result<usize, Error> {
    let mut connection = infrastructure::db::establish_connection()?;
    let now = chrono::Local::now().naive_local();
    diesel::update(action_tokens::table.filter(action_tokens::id.eq(id)))
        .set(action_tokens::executed_at.eq(now))
        .execute(&mut connection)
        .map_err(Error::from)
}
//get action token by its unique token that was sent to email
pub async fn get_action_token_by_token(token: String) -> Result<ActionToken, Error> {
    let mut connection = infrastructure::db::establish_connection()?;
    action_tokens::table
        .filter(action_tokens::token.eq(token))
        .first(&mut connection)
        .map_err(Error::from)
}
