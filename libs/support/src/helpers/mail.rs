use error::Error;
use lettre::message::header::ContentType;
use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, SmtpTransport, Transport};
use std::env;

pub async fn send_verification_mail(
    token: &String,
    user_email: &str,
    url: String,
) -> Result<usize, Error> {
    let email = Message::builder()
        .from("NoBody <nobody@domain.tld>".parse()?)
        .to(user_email.parse()?)
        .subject("Email verification")
        .header(ContentType::TEXT_PLAIN)
        .body(format!("http://{}/{}", url, token))?;

    let creds = Credentials::new(
        env::var("MAIL_USERNAME").expect("mail username must be sed"),
        env::var("MAIL_PASSWORD").expect("mail password must be set"),
    );

    // Open a remote connection to gmail
    let mailer = SmtpTransport::relay("smtp.gmail.com")?
        .credentials(creds)
        .build();

    // Send the email
    mailer.send(&email).map(|_| 1).map_err(Error::from)
}
