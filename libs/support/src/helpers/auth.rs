use bcrypt;
use chrono::{DateTime, Duration, Utc};
use error::Error;
use hmac::{Hmac, Mac};
use jwt::{SignWithKey, VerifyWithKey};
use sha2::Sha256;
use std::collections::BTreeMap;
use std::env;

/// Helper method to create hash of a password
pub fn hash_password(password: &str) -> Result<String, Error> {
    //generate password hash using default cost which is 12
    bcrypt::hash(password, bcrypt::DEFAULT_COST).map_err(Error::from)
}
//verify the input password using the hashed password
pub fn verify_password(password: &str, hash: &str) -> Result<bool, Error> {
    bcrypt::verify(password, hash).map_err(Error::from)
}

pub async fn create_access_token(user_id: &str) -> Result<String, Error> {
    let access_token_duration = env::var("ACCESS_TOKEN_DURATION_SECONDS")
        .expect("access token duration must be set")
        .parse::<i64>()
        .expect("ACCESS_TOKEN_DURATION_SECONDS is not a valid number");

    //create jwt access and refresh tokens
    let access_token_secret =
        env::var("ACCESS_TOKEN_SECRET").expect("ACCESS_TOKEN_SECRET must be set");
    //create access token key
    let access_token_key: Hmac<Sha256> = Hmac::new_from_slice(access_token_secret.as_bytes())?;
    let now = Utc::now();
    let expires_at = (now + Duration::seconds(access_token_duration)).to_rfc2822();

    //add claims for duration and user id in access token
    let mut access_token_claims = BTreeMap::new();
    access_token_claims.insert("id", user_id);
    access_token_claims.insert("expires_at", &expires_at);
    //create the access token
    access_token_claims
        .sign_with_key(&access_token_key)
        .map_err(Error::from)
}

pub async fn create_refresh_token(user_id: &str) -> Result<String, Error> {
    //create jwt access and refresh tokens
    let refresh_token_duration = env::var("REFRESH_TOKEN_DURATION_DAYS")
        .expect("refresh token duration must be set")
        .parse::<i64>()
        .expect("REFRESH_TOKEN_DURATION_DAYS is not a valid number");

    let refresh_token_secret =
        env::var("REFRESH_TOKEN_SECRET").expect("REFRESH_TOKEN_SECRET must be set");
    //create access token key
    let refresh_token_key: Hmac<Sha256> = Hmac::new_from_slice(refresh_token_secret.as_bytes())?;
    let now = Utc::now();
    let expires_at = (now + Duration::days(refresh_token_duration)).to_rfc2822();

    //add claims for duration and user id in access token
    let mut refresh_token_claims = BTreeMap::new();
    refresh_token_claims.insert("id", user_id);
    refresh_token_claims.insert("expires_at", &expires_at);
    //create the access token
    refresh_token_claims
        .sign_with_key(&refresh_token_key)
        .map_err(Error::from)
}

pub async fn verify_refresh_token(token: &str) -> Result<String, Error> {
    //get the refresh token secret from the environment
    let refresh_token_secret =
        env::var("REFRESH_TOKEN_SECRET").expect("REFRESH_TOKEN_SECRET must be set");
    //create the refresh token key
    let refresh_token_key: Hmac<Sha256> = Hmac::new_from_slice(refresh_token_secret.as_bytes())?;
    //verify the token and get the claims
    let refresh_token_claims: BTreeMap<String, String> = token
        .verify_with_key(&refresh_token_key)
        .map_err(Error::from)?;

    //check if the verified token has the keys "id" and "expires_at"
    let user_id = match refresh_token_claims.get("id") {
        Some(user_id) => user_id,
        _ => {
            return Err(Error::InternalError("internal server error".to_string()));
        }
    };

    let expires_at = match refresh_token_claims.get("expires_at") {
        Some(expires_at) => expires_at,
        _ => {
            return Err(Error::InternalError("internal server error".to_string()));
        }
    };

    //parse the expires_at value as a DateTime
    let expires_at = DateTime::parse_from_rfc2822(expires_at)?;
    //get the current time
    let now = Utc::now();

    //if the current time is bigger than the expires_at value, return an internal server error
    if now > expires_at {
        return Err(Error::Unauthorized("token expired".to_string()));
    }

    Ok(user_id.to_string())
}

pub fn verify_access_token(token: &str) -> Result<String, Error> {
    //get the access token secret from the environment
    let access_token_secret =
        env::var("ACCESS_TOKEN_SECRET").expect("ACCESS_TOKEN_SECRET must be set");
    //create the access token key
    let access_token_key: Hmac<Sha256> = Hmac::new_from_slice(access_token_secret.as_bytes())?;

    //verify the token and get the claims
    let access_token_claims: BTreeMap<String, String> = token
        .verify_with_key(&access_token_key)
        .map_err(Error::from)?;
    //check if the verified token has the keys "id" and "expires_at"
    let user_id = match access_token_claims.get("id") {
        Some(user_id) => user_id,
        _ => {
            return Err(Error::InternalError("internal server error".to_string()));
        }
    };

    let expires_at = match access_token_claims.get("expires_at") {
        Some(expires_at) => expires_at,
        _ => {
            return Err(Error::InternalError("internal server error".to_string()));
        }
    };

    //parse the expires_at value as a DateTime
    let expires_at = DateTime::parse_from_rfc2822(expires_at)?;
    //get the current time
    let now = Utc::now();

    //if the current time is bigger than the expires_at value, return an internal server error
    if now > expires_at {
        return Err(Error::Unauthorized("token expired".to_string()));
    }

    Ok(user_id.to_string())
}
