use rand::{distributions::Alphanumeric, Rng};
//generate a unique random string of length n that will be used for email verification
pub fn generate_token(n: usize) -> String {
    let rng = rand::thread_rng();
    // sample characters from the alphanumeric distribution
    rng.sample_iter(&Alphanumeric)
        // take only n characters
        .take(n)
        // convert each character to a u8 value
        .map(char::from)
        // convert each u8 value to a string
        .map(|c| c.to_string())
        // collect the strings into a single string
        .collect()
}
