use error::Error;

#[allow(dead_code)]
pub fn is_dev() -> bool {
    get_default("IS_DEV", "false") == *"true"
}
/// Get value from config or pass the default value
pub fn get_default(key: &str, default: &str) -> String {
    get(key).unwrap_or_else(|_| String::from(default))
}

/// Get configuration variable
pub fn get(key: &str) -> Result<String, Error> {
    let key = String::from(key);
    std::env::var(key).map_err(|_e| Error::NotFound)
}

