## Summary

Simple REST Api (backend application) to simulate POKEMON Pokedex. Application has user registration and login . Pokemon application
works as a type of quiz where for every Pokemon that user guesses, Pokemon will appear in users
Pokedex. List of all Pokemons can be found on an external pipe. Technologies that are used: Rust, PostgreSQL, Redis.