CREATE TABLE users (
    id          VARCHAR(36) DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    email       VARCHAR UNIQUE,
    first_name  VARCHAR,
    last_name   VARCHAR,
    password    VARCHAR,
    refresh_token VARCHAR,
    created_at  TIMESTAMPTZ DEFAULT NOW(),
    updated_at  TIMESTAMPTZ DEFAULT NOW(),
    email_verified_at TIMESTAMPTZ DEFAULT NULL
)