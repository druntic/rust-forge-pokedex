CREATE TABLE pokemons (
    id              VARCHAR(36) DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name            VARCHAR UNIQUE,
    base_experience INTEGER,
    height          INTEGER,
    pokemon_id      INTEGER,
    is_default      BOOLEAN,
    pokemon_order   INTEGER,
    image           VARCHAR,
    weight          INTEGER,
    created_at      TIMESTAMPTZ DEFAULT NOW(),
    updated_at      TIMESTAMPTZ DEFAULT NOW()
);
