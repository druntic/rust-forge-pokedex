CREATE TABLE action_tokens (
    id          VARCHAR(36) DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    entity_id   VARCHAR NOT NULL,
    token       VARCHAR NOT NULL,
    action_name VARCHAR NOT NULL,
    executed_at TIMESTAMPTZ DEFAULT NULL,
    created_at  TIMESTAMPTZ DEFAULT NOW(),
    updated_at  TIMESTAMPTZ DEFAULT NOW(),
    expires_at  TIMESTAMPTZ
)