CREATE TABLE pokemon_stats (
    id          VARCHAR(36) DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    pokemon_id  VARCHAR(36) REFERENCES pokemons(id),
    name        VARCHAR,
    base_stat   INTEGER,
    effort      INTEGER,
    created_at  TIMESTAMPTZ DEFAULT NOW(),
    updated_at  TIMESTAMPTZ DEFAULT NOW()
);
