CREATE TABLE pokemon_abilities (
    id          VARCHAR(36) DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name        VARCHAR,
    pokemon_id  VARCHAR(36) REFERENCES pokemons(id),
    created_at  TIMESTAMPTZ DEFAULT NOW(),
    updated_at  TIMESTAMPTZ DEFAULT NOW()
);
